import 'package:flutter/material.dart';
import 'package:intro_slider/dot_animation_enum.dart';

import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:school_app/Screens/Login.dart';


class Intro extends StatefulWidget {
  @override
  _IntroState createState() => _IntroState();
}

class _IntroState extends State<Intro> {
List<Slide> slides= new List();

Function goToTab;
void initState() {
  super.initState();

  slides.add(
    new Slide(
      title: "Detailed \n Insights",
      styleTitle:
      TextStyle(color: Color(0xff000000), fontSize: 30.0, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),
      description:
      "Customized feedback with recomendations at every step ",
      styleDescription:
      TextStyle(color: Colors.blue, fontSize: 20.0, fontStyle: FontStyle.italic, fontFamily: 'Raleway'),
      pathImage: "images/school1.jpg",
      widthImage: 200,
      heightImage: 200
    ),
  );
  slides.add(
    new Slide(
      title: "Personalised \n learning",

      styleTitle:
      TextStyle(color: Color(0xff000000), fontSize: 30.0, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),
      description: "Unique learning journeys created just for you!",
      styleDescription:
      TextStyle(color: Colors.blue, fontSize: 20.0, fontStyle: FontStyle.italic, ),
      pathImage: "images/school2.jpg",
        widthImage: 200,
        heightImage: 200
    ),
  );
  slides.add(
    new Slide(
      title: "Best Teachers \n in India",

      maxLineTitle: 2,
      styleTitle:
      TextStyle(color: Color(0xff000000), fontSize: 30.0, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),
      description:
      "Engaging videos that make learning simple and fun",

      styleDescription:
      TextStyle(color: Colors.blue, fontSize: 20.0, fontStyle: FontStyle.italic, fontFamily: 'Raleway'),
      pathImage: "images/teacher.jpg",
        widthImage: 200,
        heightImage: 200
    ),
  );
}
void onDonePress() {
  // Back to the first tab
  Navigator.push(
    context,
    MaterialPageRoute(
        builder: (context) => Login()),
  );
}

void onTabChangeCompleted(index) {
  // Index of current tab is focused
}

Widget renderNextBtn() {
  return Text("Next",style: TextStyle(
      color: Colors.white,fontSize: 20,
  ),);
}

Widget renderDoneBtn() {
  return Text("Done",style: TextStyle(
    color: Colors.white,fontSize: 20,
  ),);
}

Widget renderSkipBtn() {
  return Text("Skip",style: TextStyle(
      color: Colors.black,fontSize: 20,
  ),);
}

List<Widget> renderListCustomTabs() {
  List<Widget> tabs = new List();
  for (int i = 0; i < slides.length; i++) {
    Slide currentSlide = slides[i];
    tabs.add(Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.lightBlue, Colors.white]
        ),
        borderRadius: BorderRadius.circular(10)
      ),
      child: Center(
        child: Container(
          margin: EdgeInsets.only(bottom: 60.0, top: 60.0),
          child: ListView(
            children: <Widget>[
              GestureDetector(
                  child: Image.asset(
                    currentSlide.pathImage,
                    width: 200.0,
                    height: 200.0,
                    fit: BoxFit.contain,
                  )),
              Container(
                child: Text(
                  currentSlide.title,
                  style: currentSlide.styleTitle,
                  textAlign: TextAlign.center,
                ),
                margin: EdgeInsets.only(top: 20.0),
              ),
              Container(

                child: Text(
                  currentSlide.description,
                  style: currentSlide.styleDescription,
                  textAlign: TextAlign.center,
                  maxLines: 5,
                  overflow: TextOverflow.ellipsis,
                ),
                margin: EdgeInsets.only(top: 50.0,left: 60,right: 60),
              ),
            ],
          ),
        ),
      ),
    ));
  }
  return tabs;
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Colors.lightBlue, Colors.white]
            ),
        ),
        child: IntroSlider(
          // Skip button
          renderSkipBtn: this.renderSkipBtn(),
          colorSkipBtn: Colors.white,
          highlightColorSkipBtn: Colors.grey,
          // Next button
          renderNextBtn: this.renderNextBtn(),
          // Done button
          renderDoneBtn: this.renderDoneBtn(),
          onDonePress: this.onDonePress,
          colorDoneBtn: Colors.lightBlue,
          highlightColorDoneBtn: Colors.purple,

          // Dot indicator
          colorDot: Colors.blue,
          sizeDot: 8.0,
          typeDotAnimation: dotSliderAnimation.SIZE_TRANSITION,

          // Tabs
          listCustomTabs: this.renderListCustomTabs(),
          backgroundColorAllSlides: Colors.white,
          refFuncGoToTab: (refFunc) {
            this.goToTab = refFunc;
          },

          // Behavior
          scrollPhysics: BouncingScrollPhysics(),

          // Show or hide status bar
          shouldHideStatusBar: true,

          // On tab change completed
          onTabChangeCompleted: this.onTabChangeCompleted,
        ),

      ),
    );
  }
}
