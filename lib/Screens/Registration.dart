import 'package:flutter/material.dart';

import 'Home/HomePage.dart';
import 'Login.dart';
 class Register extends StatefulWidget {
   @override
   _RegisterState createState() => _RegisterState();
 }

 class _RegisterState extends State<Register> {
   final LinearGradient _gradient=LinearGradient(
       colors: <Color>[
         Colors.blue,
         Colors.lightBlueAccent
       ]
   );
   String valueChoose;
   List itemlist=["+91","+92"];
   final _formKey = GlobalKey<FormState>();
   @override
   Widget build(BuildContext context) {
     return Scaffold(
       backgroundColor: Colors.black,
       body:Form(
         key: _formKey,
         child: Container(
           width: MediaQuery.of(context).size.width,
           height: MediaQuery.of(context).size.height,
           decoration: BoxDecoration(
               color: Colors.white,
               borderRadius: BorderRadius.circular(20)
           ),
           child: SingleChildScrollView(
             child: Column(
               children: [
                 Container(
                   margin:EdgeInsets.only(top: 80),
                   alignment: Alignment.topCenter,
                   child: ShaderMask(
                     shaderCallback: (Rect rect){
                       return _gradient.createShader(rect);
                     },
                     child: Text("Access",
                       style: TextStyle(
                           fontSize: 25,
                           color: Colors.white,
                           fontFamily: 'RobotoMono'
                       ),),
                   ),
                 ),
                 Text("5000+ Engaging Videos",style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold,color: Colors.blue),),
                 SizedBox(
                   height: MediaQuery.of(context).size.height *.05,
                 ),

                Padding(
                  padding: const EdgeInsets.only(left: 30,right: 30),
                  child: TextFormField(
                       validator: (value) {
                         if (value.isEmpty) {
                           return 'Please enter Your Name';
                         }
                         return null;
                       },
                       decoration: InputDecoration(
                         hintText: "Name",
                       ),
                       keyboardType:TextInputType.name,
                     ),
                ),

                 Padding(
                   padding: const EdgeInsets.only(left: 30,right: 30),
                   child: TextFormField(
                     validator: (value) {
                       if (value.isEmpty) {
                         return 'Please enter Your Mobile Number';
                       }
                       if (value.length != 10){
                         return 'Mobile number must be of 10 digit';
                       }
                       return null;
                     },
                     decoration: InputDecoration(
                       hintText: "Enter Mobile no.",
                       icon:  Container(
                         height: 45,
                         width: 60,
                         alignment: Alignment.bottomCenter,
                         margin: EdgeInsets.only(top: 5),
                         child: DropdownButton(
                             underline: SizedBox(),
                             hint: Text("+91"),
                             value: valueChoose,
                             onChanged: (newValue){
                               setState(() {
                                 valueChoose = newValue;
                               });
                             },
                             items:itemlist.map((valueItem){
                               return DropdownMenuItem(
                                   value: valueItem,
                                   child:Text(valueItem));
                             }).toList()
                         ),
                       ),
                     ),
                     keyboardType:TextInputType.phone,

                   ),
                 ),

                    Padding(
                      padding: const EdgeInsets.only(left: 30,right: 30),
                      child: TextFormField(
                       validator: (value) {
                         if (value.isEmpty) {
                           return 'Please enter Your Email';
                         }
                         return null;
                       },
                       decoration: InputDecoration(
                         hintText: "Email",
                       ),
                       keyboardType:TextInputType.emailAddress,
                   ),
                    ),


                  Padding(
                    padding: const EdgeInsets.only(left: 30,right: 30),
                    child: TextFormField(
                       validator: (value) {
                         if (value.isEmpty) {
                           return 'Please enter valid Address';
                         }
                         return null;
                       },
                       decoration: InputDecoration(

                         hintText: "Address",
                       ),
                       keyboardType:TextInputType.streetAddress,
                     ),
                  ),

                 SizedBox(
                   height:MediaQuery.of(context).size.height*.10 ,
                 ),
                 InkWell(
                   child: Container(
                     height:MediaQuery.of(context).size.height*.07 ,
                     width: MediaQuery.of(context).size.width*.35,
                     decoration: BoxDecoration(
                         gradient: LinearGradient(
                             colors: <Color>[Colors.blue,Colors.lightBlueAccent]
                         ),
                         borderRadius: BorderRadius.circular(25)
                     ),
                     child: Center(child: Text("Register",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),)),
                   ),
                   onTap: (){
                     if (_formKey.currentState.validate()) {
                       Navigator.push(
                         context,
                         MaterialPageRoute(
                             builder: (context) => Home6th()),
                       );
                     }
                   },
                 ),
                 SizedBox(
                   height:MediaQuery.of(context).size.height *.20,
                 ),
                 Row(
                   children: [
                     SizedBox(width: MediaQuery.of(context).size.width*.05 ,),
                     Text("Already registered?",style: TextStyle(
                         fontSize: 12,
                         fontWeight: FontWeight.bold,
                         color: Colors.grey,
                         fontFamily: 'RobotoMono'
                     ),),
                     InkWell( onTap:
                     (){ Navigator.push(
                       context,
                       MaterialPageRoute(
                           builder: (context) => Login()),
                     );},
                       child: ShaderMask(
                       shaderCallback: (Rect rect){
                         return _gradient.createShader(rect);
                       },
                       child: Text("Login",
                         style: TextStyle(
                             fontSize: 12,
                             fontWeight: FontWeight.bold,
                             color: Colors.white,
                             fontFamily: 'RobotoMono'
                         ),),
                     ),),
                     SizedBox(
                       width:MediaQuery.of(context).size.width *.15
                     ),
                      Text("Privacy Policy and T&C",style: TextStyle(fontSize: 12,decoration: TextDecoration.underline), )
                   ],
                 ),
                 SizedBox(height:MediaQuery.of(context).size.height *.03,)
               ],
             ),
           ),
         ),
       ),
     );
   }
 }
