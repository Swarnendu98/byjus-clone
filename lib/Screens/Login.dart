import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'Home/HomePage.dart';
import 'Select.dart';


class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
final LinearGradient _gradient=LinearGradient(
  colors: <Color>[
    Colors.blue,
    Colors.lightBlueAccent
  ]
);
String valueChoose;
List itemlist=["+91","+92"];
final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Form(
        key: _formKey,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
         color: Colors.white,
              borderRadius: BorderRadius.circular(20)
          ),


          child: Column(
            children: <Widget>[
              Container(
                margin:EdgeInsets.only(top: 100),
                alignment: Alignment.topCenter,
                child: ShaderMask(
                  shaderCallback: (Rect rect){
                    return _gradient.createShader(rect);
                  },
                  child: Text("LOGIN",
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                        fontFamily: 'RobotoMono'
                    ),),
                ),
              ),
              Text("Great to see you again!",style: TextStyle(color: Colors.grey),),
              SizedBox(
                height:MediaQuery.of(context).size.height *.05,
              ),
               Padding(
                 padding: const EdgeInsets.only(left: 60,right: 60),
                 child: TextFormField(

                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Your Mobile Number';
                          }
                          if (value.length != 10){
                            return 'Mobile number must be of 10 digit';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          hintText: "Enter Mobile no.",

                          icon:  Container(
                            height: 45,
                            width: 60,
                            alignment: Alignment.bottomCenter,
                            margin: EdgeInsets.only(top: 5),
                            child: DropdownButton(
                                underline: SizedBox(),
                                hint: Text("+91",style: TextStyle(fontSize: 20),),
                                value: valueChoose,
                                onChanged: (newValue){
                                  setState(() {
                                    valueChoose = newValue;
                                  });
                                },
                                items:itemlist.map((valueItem){
                                  return DropdownMenuItem(
                                      value: valueItem,
                                      child:Text(valueItem));
                                }).toList()
                            ),
                          ),
                        ),
                        keyboardType:TextInputType.phone,
                   
                      ),
               ),
              SizedBox(
                height:MediaQuery.of(context).size.height*.10,
              ),
              Text("We will send a 4 digit OTP to verify",style: TextStyle(color: Colors.grey,),),
              SizedBox(
                height:MediaQuery.of(context).size.height*.03 ,
              ),
              InkWell(
                child: Container(
                  height:MediaQuery.of(context).size.height*.07 ,
                  width: MediaQuery.of(context).size.width*.35,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: <Color>[Colors.blue,Colors.lightBlueAccent]
                    ),
                    borderRadius: BorderRadius.circular(25)
                  ),
                  child: Center(child: Text("Next",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),)),
                ),
                onTap: (){
                  if (_formKey.currentState.validate()) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Home6th()),
                    );
                  }
                },
              ),
              SizedBox(
                height:MediaQuery.of(context).size.height*.30 ,
              ),
              Text("Create new account",style: TextStyle(color: Colors.grey,),),
              TextButton(
                  onPressed:(){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SelectCourse()),
                    );
                  },
                  child:ShaderMask(
                    shaderCallback: (Rect rect){
                      return _gradient.createShader(rect);
                    },
                    child: Text("Register",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontFamily: 'RobotoMono'
                      ),),
                  ), )

            ],
          ),
        ),
      ),
    );
  }
}
