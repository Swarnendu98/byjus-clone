import 'package:flutter/material.dart';
import 'package:school_app/Screens/Registration.dart';

class SelectCourse extends StatefulWidget {
  @override
  _SelectCourseState createState() => _SelectCourseState();
}

class _SelectCourseState extends State<SelectCourse> {
  final LinearGradient _gradient=LinearGradient(
      colors: <Color>[
        Colors.blue,
        Colors.lightBlueAccent
      ]
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body:Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
           color: Colors.white,
            borderRadius: BorderRadius.circular(20)


        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height:MediaQuery.of(context).size.height*.10 ,
              ),
               ShaderMask(
                   shaderCallback: (Rect rect){
                     return _gradient.createShader(rect);
                   },
                   child: Text("Choose",style: TextStyle(fontSize: 25,color: Colors.white),)),
              Text("your course",style: TextStyle(fontSize: 25,color: Colors.blue,fontWeight: FontWeight.bold),),
              SizedBox(
                height:MediaQuery.of(context).size.height*.02 ,
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                  alignment: Alignment.center,
                  child: Text("Classes 4th-12th",style: TextStyle(fontSize: 18,color: Colors.black,fontWeight: FontWeight.bold),)),
              Wrap(
                direction: Axis.horizontal,
                alignment: WrapAlignment.start,
                spacing: 20,
                runSpacing: 20,
                children: <Widget>[
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("4th",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("5th",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("6th",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Register()),
                      );
                    },
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("7th",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("8th",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("9th",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("10th",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("11th",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("12th",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.75,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("11th Grade-Commerce",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.75,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),

                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("12th Grade-Commerce",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),


                ],
              ),
              SizedBox(
                height:MediaQuery.of(context).size.height*.02 ,
              ),
              Container(
                  margin: EdgeInsets.only(bottom: 20,top: 10),
                  alignment: Alignment.center,
                  child: Text("KG-CLASSES 3",style: TextStyle(fontSize: 18,color: Colors.black,fontWeight: FontWeight.bold),)),
              SizedBox(
                height:MediaQuery.of(context).size.height*.02 ,
              ),
              Wrap(
                spacing: 15,
                runSpacing: 15,
                children: [
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("LKG",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("UKG",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("1st",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("2nd",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("3rd",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height:MediaQuery.of(context).size.height*.02 ,
              ),
              Container(
                  margin: EdgeInsets.only(bottom: 20,top: 10),
                  alignment: Alignment.center,
                  child: Text("EXAM PREPARATION",style: TextStyle(fontSize: 18,color: Colors.black,fontWeight: FontWeight.bold),)),
              SizedBox(
                height:MediaQuery.of(context).size.height*.02 ,
              ),
              Wrap(
                spacing: 15,
                runSpacing: 15,
                children: [
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("IAS",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("CAT",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width*.25,
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("GMAT",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height:MediaQuery.of(context).size.height*.07 ,
                      width: MediaQuery.of(context).size.width*.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: const Offset(
                                5.0,
                                5.0,
                              ),
                              blurRadius: 3.0,
                              spreadRadius: 1.0,
                            ), //BoxShadow
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),]
                      ),
                      child: Center(child: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Text("GRE",style: TextStyle(color: Colors.white,fontSize: 20),))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height:MediaQuery.of(context).size.height*.02 ,
              ),
            ],
          ),
        ),
      )
    );
  }
}
