import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class Home4th extends StatefulWidget {
  @override
  _Home4thState createState() => _Home4thState();
}

class _Home4thState extends State<Home4th> {
  GlobalKey<ScaffoldState> _globalKey= GlobalKey<ScaffoldState>();
  final LinearGradient _gradient=LinearGradient(
      colors: <Color>[
        Colors.blue,
        Colors.lightBlueAccent
      ]
  );
  final LinearGradient _gradient1=LinearGradient(

      colors: <Color>[
        Colors.green,
        Colors.lightGreen
      ]
  );
  final LinearGradient _gradient2=LinearGradient(

      colors: <Color>[
        Colors.deepOrangeAccent,
        Colors.yellow
      ]
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      key: _globalKey,
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(child:Container(
              child: Center(
                child: Wrap(
                  direction: Axis.horizontal,
                  spacing: 30,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        shape:BoxShape.circle,
                      ),

                      child: CircleAvatar(
                        radius: 25,
                      ),
                    ),

                    Container(
                      child: Column(
                        children: [
                          Text("Swarnendu",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                          Text("4th grade")
                        ],
                      ),

                    ),


                    Container(
                      decoration: BoxDecoration(
                        shape:BoxShape.circle,
                        gradient: LinearGradient(
                            colors: <Color>[Colors.blue,Colors.lightBlueAccent]
                        ),
                      ),
                      child: CircleAvatar(
                        backgroundColor:Colors.transparent,
                        radius: 18,
                        child: IconButton(
                          icon: Icon(Icons.arrow_forward,color: Colors.white ,),
                          iconSize: 18,
                          color: Colors.white,
                          onPressed: (){},
                        ),
                      ),

                    )
                  ],
                ),
              ),
            )),
            SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height*.65,
                child: ListView(
                  children: [
                    ListTile(
                      title: Text("ABC Classes"),
                      trailing: Container(
                        width: 90,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [Colors.yellow,Colors.red]
                            )
                        ),
                        child: Center(child: Text("Know more")),
                      ),
                      leading: ShaderMask(
                        shaderCallback: (Rect rect){
                          return _gradient.createShader(rect);
                        },
                        child: Icon(
                          CupertinoIcons.person_crop_rectangle_fill,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    ListTile(
                      title: Text("Learning Analysis"),
                      leading: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Icon(Icons.analytics,color: Colors.white,)),
                    ),
                    ListTile(
                      title: Text("Bookmarks"),
                      leading: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },child: Icon(Icons.bookmark,color: Colors.white,)),
                    ),
                    ListTile(
                      title: Text("Parent Connect"),
                      leading: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },child: Icon(CupertinoIcons.person_2_alt,color: Colors.white,)),
                    ),
                    ListTile(
                      title: Text("Notifications"),
                      leading: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },child: Icon(Icons.notifications,color: Colors.white,)),
                    ),
                    ListTile(
                      title: Text("Badges"),
                      leading: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },child: Icon(CupertinoIcons.shield_fill,color: Colors.white,)),
                    ),
                    ListTile(
                      title: Text("Quizzo"),
                      leading: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },child: Icon(CupertinoIcons.question_circle_fill,color: Colors.white,)),
                    ),
                    ListTile(
                      title: Text("BYJU'S Young Genius"),
                      leading: ShaderMask(
                        shaderCallback: (Rect rect){
                          return _gradient.createShader(rect);
                        },
                        child: Icon(
                          CupertinoIcons.lightbulb_fill,color: Colors.white,
                        ),
                      ),
                    ),

                    ListTile(
                      title: Text("School Super League"),
                      leading: ShaderMask(
                        shaderCallback: (Rect rect){
                          return _gradient.createShader(rect);
                        },
                        child: Icon(
                          Icons.leaderboard,color: Colors.white,
                        ),
                      ),
                    ),
                    ListTile(
                      title: Text("Share the app"),
                      leading: ShaderMask(
                        shaderCallback: (Rect rect){
                          return _gradient.createShader(rect);
                        },
                        child: Icon(
                          Icons.share,color: Colors.white,
                        ),
                      ),
                    ),
                    ListTile(
                      title: Text("Contact Us"),
                      leading: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Icon(CupertinoIcons.phone,color: Colors.white,)),
                    ),
                    ListTile(
                      title: Text("Redeem Voucher"),
                      leading: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Icon(Icons.wallet_giftcard_sharp,color: Colors.white,)),
                    ),
                    ListTile(
                      title: Text("Subscribe now"),
                      leading: ShaderMask(
                          shaderCallback: (Rect rect){
                            return _gradient.createShader(rect);
                          },
                          child: Icon(Icons.download_done_outlined,color: Colors.white,)),
                    ),
                    ListTile(
                      title: Text("Terms & Conditions"),
                      leading: ShaderMask(
                        shaderCallback: (Rect rect){
                          return _gradient.createShader(rect);
                        },
                        child: Icon(
                          Icons.info,color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width*.20,
              height: MediaQuery.of(context).size.height*.08,
              margin: EdgeInsets.only(left: 10,right: 10,bottom: 10),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: <Color>[Colors.blue,Colors.lightBlueAccent]
                  ),
                  borderRadius: BorderRadius.circular(25)
              ),

              child: Center(
                child: Wrap(
                  spacing: 10,
                  children: [
                    Icon(Icons.wifi_calling_sharp,color: Colors.white,),
                    Text("Enquire Now",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 18),)
                  ],
                ),
              ),

            )

          ],
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20)
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.all(20),
                child: Row(
                  children: [
                    CircleAvatar(
                      radius: 20,
                      backgroundColor: Colors.white,
                      child: IconButton(
                        onPressed: ()=>_globalKey.currentState.openDrawer(),
                        icon: Icon(Icons.menu,color: Colors.black,),
                        iconSize: 14,
                      ),
                    ),
                    SizedBox(
                      width:MediaQuery.of(context).size.width *.10,
                    ),
                    Center(
                      child: Text("SCHOOL APP"),
                    ),
                    SizedBox(
                      width:MediaQuery.of(context).size.width *.08,
                    ),
                    Expanded(
                      child: Container(
                        width:MediaQuery.of(context).size.width *.26 ,
                        height:MediaQuery.of(context).size.height *.05  ,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [Colors.yellow,Colors.pinkAccent]
                            ),
                            borderRadius: BorderRadius.circular(30)
                        ),
                        child: Center(child:Row(children: [
                          SizedBox(width: 15,),
                          Icon(
                            CupertinoIcons.person_crop_rectangle_fill,
                            color: Colors.white,
                            size: 18,
                          ),
                          SizedBox(width: 14,),
                          Text("CLASSES",style: TextStyle(color:Colors.white),)])),
                      ),
                    )
                  ],
                ),
              ),
              Wrap(
                direction: Axis.horizontal,
                spacing: 15,
                runSpacing: 12,
                children: [
                  Container(
                    width:MediaQuery.of(context).size.width *.26 ,
                    height:MediaQuery.of(context).size.height *.20,
                    child: Column(
                      children: [
                        Container(

                          decoration:BoxDecoration(
                              shape: BoxShape.circle,
                              gradient: _gradient2
                          ),
                          child: CircleAvatar(
                              backgroundColor: Colors.transparent,
                              radius: 40,

                              child:IconButton(
                                icon: FaIcon(FontAwesomeIcons.draftingCompass,size: 30,color: Colors.white,),
                              )

                          ),
                        ),
                        Text("Mathematics")
                      ],
                    ),
                  ),
                  Container(
                    width:MediaQuery.of(context).size.width *.26 ,
                    height:MediaQuery.of(context).size.height *.20,
                    child: Column(
                      children: [
                        Container(
                          decoration:BoxDecoration(
                              shape: BoxShape.circle,
                              gradient: LinearGradient(
                                  colors: [
                                    Colors.pink,
                                    Color(0xffFFB6C1)
                                  ]
                              )
                          ),
                          child: CircleAvatar(
                              radius: 40,
                              backgroundColor: Colors.transparent,
                              child: IconButton(
                                icon: FaIcon(FontAwesomeIcons.atom,size: 30,color: Colors.white,),
                              )
                          ),
                        ),

                        Text("Science")
                      ],
                    ),
                  ),

                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width*.90,
                height: 1,
                color: Colors.grey,
                margin: EdgeInsets.all(10),
              ),
              Container(
                width: MediaQuery.of(context).size.width*.90,
                height: MediaQuery.of(context).size.height*.30,
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("images/class.jpg"),
                      fit:BoxFit.cover
                  ),
                  borderRadius: BorderRadius.circular(15),

                ),

                child: Container(
                  margin: EdgeInsets.only(top:20,right: 10),
                  alignment: Alignment.centerRight,
                  child: Column(
                    children: [
                      Text("BYJU'S Classes",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white,fontSize: 16),),
                      SizedBox(height: 5,),
                      Text("Online tution by inida's \nbest teachers with live \ndoubt solving and \none-on-one attention",style: TextStyle(fontSize: 13,color: Colors.white),),
                      SizedBox(height: 15,),
                      Container(
                        width: MediaQuery.of(context).size.width*.35,
                        height: MediaQuery.of(context).size.height*.05,

                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.white,
                        ),
                        child: Center(
                          child: ShaderMask(
                            shaderCallback: (Rect rect){
                              return _gradient.createShader(rect);
                            },
                            child: Text("Book free trial",style: TextStyle(color: Colors.white  ),),
                          ),
                        ),
                      )

                    ],
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width*.90,
                height: 1,
                color: Colors.grey,
                margin: EdgeInsets.all(10),
              ),
              Container(
                width: MediaQuery.of(context).size.width*.80,
                height:20,
                child: Text("Quizzo",style: TextStyle(
                    fontWeight: FontWeight.bold
                ),),
              ),
              Container(
                  width: MediaQuery.of(context).size.width*.90,
                  height:120,
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey[300],
                          offset: const Offset(
                            5.0,
                            5.0,
                          ),
                          blurRadius: 10.0,
                          spreadRadius: 2.0,
                        ), //BoxShadow
                        BoxShadow(
                          color: Colors.white,
                          offset: const Offset(0.0, 0.0),
                          blurRadius: 0.0,
                          spreadRadius: 0.0,
                        ),]
                  ),
                  child:Center(
                    child: Row(
                      children: [
                        Image.asset("images/quiz.jpg",height: 80,width: 80,),
                        Container(
                          width: MediaQuery.of(context).size.width*.45,
                          margin: EdgeInsets.only(top: 20,left: 10,right: 20),
                          child: Column(
                            children: [
                              ShaderMask(
                                  shaderCallback: (Rect rect){
                                    return _gradient.createShader(rect);
                                  },
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text("Play Quizzo \nchallenge!",style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white
                                    ),),
                                  )),
                              SizedBox(height: 5,),
                              Text("Match your wits with other students?Play Quizzo now!",style: TextStyle(
                                  fontSize: 13.8
                              ),)
                            ],
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            shape:BoxShape.circle,
                            gradient: LinearGradient(
                                colors: <Color>[Colors.blue,Colors.lightBlueAccent]
                            ),
                          ),
                          child: CircleAvatar(
                            backgroundColor:Colors.transparent,
                            radius: 22,
                            child: IconButton(
                              icon: Icon(Icons.arrow_forward,color: Colors.white ,),
                              iconSize: 22,
                              color: Colors.white,
                              onPressed: (){},
                            ),
                          ),

                        )
                      ],
                    ),
                  )
              ),
              Container(
                width: MediaQuery.of(context).size.width*.90,
                height: 1,
                color: Colors.grey,
                margin: EdgeInsets.all(10),
              ),
              Container(
                width: MediaQuery.of(context).size.width ,
                height: 200,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),

                ),
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Container(
                        width: MediaQuery.of(context).size.width*.65,
                        height: MediaQuery.of(context).size.height*.30,
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("images/lv.jpg"),
                                fit:BoxFit.cover
                            ),

                            borderRadius: BorderRadius.circular(15)
                        ),
                        child: Container(

                          margin: EdgeInsets.only(left: 10),
                          alignment: Alignment.bottomLeft,

                          child: Row(
                            children: [
                              Icon(
                                Icons.menu_book,
                                color: Colors.white,
                              )
                            ],
                          ),
                        )),
                    Container(
                        width: MediaQuery.of(context).size.width*.65,
                        height: MediaQuery.of(context).size.height*.30,
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("images/lv1.jpg"),
                                fit:BoxFit.cover
                            ),
                            borderRadius: BorderRadius.circular(15)
                        ),
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          alignment: Alignment.bottomLeft,
                          child: Row(
                            children: [
                              Icon(
                                Icons.menu_book,
                                color: Colors.white,
                              )
                            ],
                          ),
                        )
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width*.65,
                        height: MediaQuery.of(context).size.height*.30,
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("images/lv2.jpg"),
                                fit:BoxFit.cover
                            ),
                            borderRadius: BorderRadius.circular(15)
                        ),
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          alignment: Alignment.bottomLeft,
                          child: Row(
                            children: [
                              Icon(
                                Icons.menu_book,
                                color: Colors.white,
                              )
                            ],
                          ),
                        )
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height*.30,
                      margin: EdgeInsets.only(left: 20,right: 20),
                      child:  Column(
                        children: [
                          Container(
                            margin:EdgeInsets.only(top: 65,left: 10,right: 10,bottom: 10),
                            decoration: BoxDecoration(
                              shape:BoxShape.circle,
                              gradient: LinearGradient(
                                  colors: <Color>[Colors.blue,Colors.lightBlueAccent]
                              ),
                            ),
                            child: CircleAvatar(
                              backgroundColor:Colors.transparent,
                              radius: 22,
                              child: IconButton(
                                icon: Icon(Icons.arrow_forward,color: Colors.white ,),
                                iconSize: 22,
                                color: Colors.white,
                                onPressed: (){},
                              ),
                            ),
                          ),
                          ShaderMask(
                              shaderCallback: (Rect rect){
                                return _gradient.createShader(rect);
                              },
                              child: Text("See All",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),))
                        ],
                      ),

                    )
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width*.90,
                height: 1,
                color: Colors.grey,
                margin: EdgeInsets.all(10),
              ),
              Container(
                width: MediaQuery.of(context).size.width*.80,
                height:20,
                child: Text("Share the app",style: TextStyle(
                    fontWeight: FontWeight.bold
                ),),
              ),
              Container(
                  width: MediaQuery.of(context).size.width*.90,
                  height:120,
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey[300],
                          offset: const Offset(
                            5.0,
                            5.0,
                          ),
                          blurRadius: 10.0,
                          spreadRadius: 2.0,
                        ), //BoxShadow
                        BoxShadow(
                          color: Colors.white,
                          offset: const Offset(0.0, 0.0),
                          blurRadius: 0.0,
                          spreadRadius: 0.0,
                        ),]
                  ),
                  child:Center(
                    child: Row(
                      children: [
                        Image.asset("images/share.jpg",height: 80,width: 80,),
                        Container(
                          width: MediaQuery.of(context).size.width*.45,
                          margin: EdgeInsets.only(top: 20,left: 10,right: 20),
                          child: Column(
                            children: [
                              ShaderMask(
                                  shaderCallback: (Rect rect){
                                    return _gradient.createShader(rect);
                                  },
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text("Share with friends",style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white
                                    ),),
                                  )),
                              SizedBox(
                                height: 5,
                              ),
                              Text("Help your friends fall in love with learning through Biyu's!",style: TextStyle(
                                  fontSize: 12
                              ),)
                            ],
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            shape:BoxShape.circle,
                            gradient: LinearGradient(
                                colors: <Color>[Colors.blue,Colors.lightBlueAccent]
                            ),
                          ),
                          child: CircleAvatar(
                            backgroundColor:Colors.transparent,
                            radius: 22,
                            child: IconButton(
                              icon: Icon(Icons.arrow_forward,color: Colors.white ,),
                              iconSize: 22,
                              color: Colors.white,
                              onPressed: (){},
                            ),
                          ),

                        )
                      ],
                    ),
                  )
              ),
            ],
          ),
        ),

      ),
    );
  }
}
